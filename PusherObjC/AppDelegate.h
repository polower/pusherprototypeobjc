//
//  AppDelegate.h
//  PusherObjC
//
//  Created by José García on 03/05/16.
//  Copyright © 2016 José García. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Pusher/Pusher.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, PTPusherDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

