//
//  AppDelegate.m
//  PusherObjC
//
//  Created by José García on 03/05/16.
//  Copyright © 2016 José García. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@property(nonatomic, strong) NSMutableArray *clients;

@end

NSString *EVENT_NAME = @"my_event";
NSString *CHANNEL_NAME = @"test_channel";
NSString *APP_KEY = @"8ef05aa0498993e2cd97";

@implementation AppDelegate

/*
 
 app_id = "203210"
 key = "cec007625c6473e6b23a"
 secret = "2203bc78a44156cd9f51"
 cluster = "eu"
 
 */

-(void) pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection failedWithError:(NSError *)error {
    NSLog(@"Connection error: %@", [error localizedDescription]);
}

-(void)pusher:(PTPusher *)pusher connectionDidConnect:(PTPusherConnection *)connection {
    if ([self.clients containsObject:pusher]) {
        NSLog(@"Client ready!");
    }
}

-(void)pusher:(PTPusher *)pusher didSubscribeToChannel:(PTPusherChannel *)channel {
    NSLog(@"didSubscribeToChannel: %@", channel.name);
}

-(void)pusher:(PTPusher *)pusher didReceiveErrorEvent:(PTPusherErrorEvent *)errorEvent {
    NSLog(@"ERROR!!");
}

-(void)pusher:(PTPusher *)pusher didUnsubscribeFromChannel:(PTPusherChannel *)channel {
    NSLog(@"didUnsubscribeFromChannel");
}

/*
 	When subscribing to private or presence channels the subscription has to be authenticated via a server.
    The URL set via the authorizationURL defines the authentication endpoint.
 */

- (void)createClient:(NSNumber *)clientNumber {
    
    if (!self.clients) {
        self.clients = [NSMutableArray new];
    }
    
    
    PTPusher *client = [PTPusher pusherWithKey:APP_KEY delegate:self encrypted:YES cluster:@"eu"];
    [client connect];
    
    PTPusherChannel *channel = [client subscribeToChannelNamed:CHANNEL_NAME];
    
    [channel bindToEventNamed:EVENT_NAME handleWithBlock:^(PTPusherEvent *channelEvent) {
        NSLog(@"%@", channelEvent.data);
    }];
    
    // IMPORTANT! Keep a STRONG reference!
    [self.clients addObject:client];
    
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self createClient:@(1)];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
